
# Crée un projet utilisant Flask pour mettre en place un serveur web simple.
from flask import Flask
from flask import jsonify

app = Flask(__name__)

# Développe des routes qui renvoient du texte et des objets JSON.
@app.route('/')
def hello_world():

    return 'Hello, World!'

@app.route('/data')
def data():
    return jsonify({'name': 'Pikachu', 'power': 20, 'life': 50})
    

